const fs = require('fs')

exports.readingPromise = (filePath) =>{
    return new Promise((resolve,reject) =>{
        fs.readFile(filePath,'utf8',(err,data)=>{
            if(err){
                reject(err)
            }
            else{
                resolve(data)
            }
        })
    })
}

exports.writeingPromise = (filePath,content) =>{
    return new Promise((resolve,reject)=>{
        fs.writeFile(filePath,content,(err,data)=>{
            if(err){
                reject(err)
            }
            else{
                resolve(filePath)
            }
        })
    })
}

exports.appendingPromise = (fileNamesPath,filePath)=>{
    return new Promise((resolve,reject)=>{
        fs.appendFile(fileNamesPath,filePath+'/n',(err,data)=>{
            if(err){
                reject(err)
            }
            else{
                resolve(filePath)
            }
        })
    })
}

exports.deletingPromise = (fileNames)=>{
    return new Promise((resolve,reject)=>{
        fileNames.forEach((eachPath) => {
            fs.unlink(eachPath,(err,data)=>{
                if(err){
                    reject(err)
                }
                else{
                    resolve('Task success')
                }
            })
        });
    })
}