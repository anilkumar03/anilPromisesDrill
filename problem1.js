const fs = require('fs')

exports.createFolderPromise = (folderPath)=>{
    return new Promise((resolve,reject)=>{
        fs.mkdir(folderPath,(err)=>{
            if(err){
                reject(err)
            }
            else{
                resolve()
            }
        })
    })
}

exports.createFilePromise = (filePath,content)=>{
    return new Promise((resolve,reject)=>{
        fs.writeFile(filePath,content,(err)=>{
            if(err){
                reject(err)
            }
            else{
                resolve()
            }
        })
    })
}

exports.deleteFilePromise = (filePath)=>{
    return new Promise((resolve,reject)=>{
        fs.unlink(filePath,(err,data)=>{
            if(err){
                reject(err)
            }
        })
    })
}

exports.deleteFolderPromise = (folderPath)=>{
    return new Promise((resolve,reject)=>{
        fs.rmdir(folderPath,(err,data)=>{
            if(err){
                reject(err)
            }
        })
    })
}

