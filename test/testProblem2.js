const path = require('path')

const test = require('../problem2')

const dataPath = path.resolve(__dirname,'../lipsum.txt')
const fileNamesPath = path.resolve(__dirname,'../fileNames.txt')
const upperCasePath = path.resolve(__dirname,'../upperCaseContent.txt')
const splitedLowerCasePath = path.resolve(__dirname,'../splitedLowerCase.txt')
const sortedPath = path.resolve(__dirname,'../sortedContent.txt')

test.readingPromise(dataPath)
.then((data1) => {
    test.writeingPromise(upperCasePath,data1.toUpperCase())
    test.appendingPromise(fileNamesPath,upperCasePath)
    return test.readingPromise(upperCasePath)
})
.then((upperData) => {
    test.writeingPromise(splitedLowerCasePath,JSON.stringify(upperData.toLowerCase().split('.')))
    test.appendingPromise(fileNamesPath,splitedLowerCasePath)
    return test.readingPromise(splitedLowerCasePath)
})
.then((splitedData) => {
    test.writeingPromise(sortedPath,JSON.parse(splitedData).sort())
    test.appendingPromise(fileNamesPath,sortedPath)
    return test.readingPromise(fileNamesPath)
})
.then((namesData) => {
    test.deletingPromise(namesData.split('/n'))
 })
.catch((err)=>{console.log(err)})

