const path = require('path')
const test = require('../problem1')

const folderPath = path.resolve(__dirname,'../randomJsonFiles')


test.createFolderPromise(folderPath)

for (let i=1; i<=5; i++){
    let filePath = path.resolve(__dirname,'../randomJsonFiles/'+`file${i}.json`)
    test.createFilePromise(filePath,i)
}

for(let i=1; i<=5; i++){
    let filePath = path.resolve(__dirname,'../randomJsonFiles/'+`file${i}.json`)
    test.deleteFilePromise(filePath)
}

test.deleteFolderPromise(folderPath)

